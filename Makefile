all: sws
debug: dbg
cb: cleanbuild

sws: sws.c
	gcc sws.c -pthread -std=gnu99 -o sws

dbg: sws.c
	gcc sws.c -g -pthread -std=gnu99 -o sws

clean:
	-rm -rf *.o *.exe sws.!* sws

cleanbuild: 
	make clean
	make dbg