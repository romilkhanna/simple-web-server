#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <time.h>
#include <sys/stat.h>
#include <ctype.h>

/*
 * Struct to encapsulate the HTTP request and response
 */
typedef struct {
	char *request; // GET, POST, etc.
    char *version; // HTTP/1.0 or HTTP/1.1
	char *uri; // the file requested, '/' means index.html
    int statusCode; // error code
    char *response; // OK or reason of the problem
} http;

void parse(char *source, char **args, char *seperator);
void setHomeDirectory(char *path);
bool checkDirExists(char *path);
bool checkFileExists(char *path);
int createServerSocket(int port, struct sockaddr_in *serverAddr);
void *startServer(void *port);
http formRequest(char *buffer);
void sendFile(int serverfd, struct sockaddr_in clientAddr, socklen_t clientAddrLength, http req);

char *homedir, input;
char *DELIMITER = "\r\n";

int main(int argc, char **argv) {
    if (argc != 3) {
    	printf("Usage: %s <port> <homedir>%s", argv[0], DELIMITER);
    	if (argc == 2) {
            fprintf(stderr, "No homedir provided%s", DELIMITER);
    	}

    	if (argc == 1) {
    		fprintf(stderr, "No port and homedir provided%s", DELIMITER);
    	}
        exit(EXIT_FAILURE);
    }
    
    int port = atoi(argv[1]);
    homedir = getcwd(NULL, 0);
    setHomeDirectory(argv[2]); // set the root directory for the web server
    

    // fork a thread that will listen to requests and respond
    pthread_t child;
    pthread_create(&child, NULL, &startServer, (void *) &port);

    // continously monitor the shell to make sure that the user doesn't want to exit.
    while (true) {
        scanf("%c", &input);
        if (input == 'q') {
            printf("Shutting down ... waiting for thread.%s", DELIMITER);
	        pthread_join(child, NULL);
			free(homedir);
			break;
		}
	}

    exit(EXIT_SUCCESS);
}

/*
 * This method takes the input line and tokenizes it
 * based on a specified delimiter. It stores the tokens in a list.
 */

void parse(char *line, char **argList, char *seperator) {
    char *tmp = strtok(line, seperator); // get all tokens

    // run while there are tokens
    int i;
    for (i = 0; tmp != NULL; i++) {
    	argList[i] = tmp;
    	tmp = strtok(NULL, seperator);
    }
    argList[i] = NULL; // mark the end
}

/*
 * This method sets the root directory for the web server.
 * It checks whether you are trying to make sure that you can
 * only set a sub directory.
 */

void setHomeDirectory(char *directory) {
    char *subDir[15];
    parse(directory, subDir, "/");
    // TODO make this more robust
    if (strcmp(subDir[0], "..") == 0) {
        fprintf(stderr, "Cannot exit the root directory%s", DELIMITER);
        exit(EXIT_FAILURE);
    }
    if (checkDirExists(strncat(homedir, directory, sizeof(directory))) == true) {
        chdir(homedir);
    } else {
        fprintf(stderr, "Error openening path %s. Make sure the directory exists in the root directory.%s", directory, DELIMITER);
        exit(EXIT_FAILURE);
    }
}

/*
 * This method checks if a given directory exists. Assumes that it doesn't.
 */

bool checkDirExists(char *path) {
    struct stat s;
    stat(path, &s);
    if (S_ISDIR(s.st_mode)) {
        return true;
    } else {
        return false;
    }
    return false;
}

/*
 * This method checks if a given file exists. Assumes that it doesn't.
 */

bool checkFileExists(char *path) {
    struct stat s;
    stat(path, &s);
    if (S_ISREG(s.st_mode)) {
        return true;
    } else {
        return false;
    }
    return false;
}

/*
 * This method creates and binds a socket for the server.
 * It binds the socket to all interfaces (IP = 0.0.0.0) so that
 * the user can connect to any IP that is available on the machine.
 */

int createServerSocket(int port, struct sockaddr_in *servAddr) {
    int serverfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (serverfd == -1) {
        fprintf(stderr, "Error creating server socket!%s", DELIMITER);
        exit(EXIT_FAILURE);
    }

    memset(servAddr, 0, sizeof(servAddr));

    servAddr->sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr->sin_family = AF_INET;
    servAddr->sin_port = htons(port);

    if (bind(serverfd, (struct sockaddr *) servAddr, sizeof(*servAddr)) == -1) {
        fprintf(stderr, "Error binding socket!%s", DELIMITER);
        close(serverfd);
        exit(EXIT_FAILURE);
    }

    return serverfd;
}

/*
 * This method is for the child thread of main. It listens on a created socket
 * given a port and then responds to the request on that socket. The response is
 * is done via a forked process so that additional requests can be handled at the
 * same time. There is also a check for quit before any additional requests are
 * handled.
 *
 * The requests received are checked for sanity before responding to the client.
 */

void *startServer(void *p) {
	int *port = (int *) p;
    struct sockaddr_in serverAddr, clientAddr;

    int serverfd = createServerSocket(*port, &serverAddr);
    socklen_t clientAddrLength = sizeof(clientAddr);
    
    printf("sws is running on UDP port %d, %s IP, and serving %s%s", *port, inet_ntoa(serverAddr.sin_addr), 
        homedir, DELIMITER);
    printf("Press 'q' to quit ...%s", DELIMITER);

    char *requestLine, *timeBuffer, *output;
	requestLine = (char *) malloc(sizeof(char) * 1024);
	timeBuffer = (char *) malloc(sizeof(char) * 80);
	output = (char *) malloc(sizeof(char) * 1024);

	while (true) {
		if (input == 'q') {
            printf("Closing socket and releasing resources ...%s", DELIMITER);
			close(serverfd);
			break;
		}

		int bytes = recvfrom(serverfd, requestLine, 1024, 0, (struct sockaddr *) &clientAddr, &clientAddrLength);

        // Shortest request is at least 3 chars
		if (bytes > 4) { 
			requestLine[bytes - 1] = '\0';
			http req = formRequest(requestLine); // Check to see if the request is good.

			time_t rawtime;
			time(&rawtime);
			strftime(timeBuffer, 80, "%b %d %T", localtime(&rawtime)); // Create the time.

            // Create the output for the log, NOTE: the log preserves what the client sent exactly.
			sprintf(output, "%s %s:%d %s; %s %d %s; %s", timeBuffer, inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port),
				requestLine, req.version, req.statusCode, req.response, req.uri);

			printf("%s%s", output, DELIMITER); // Print the log.

            // Fork a child to respond to the request and continue.
            pid_t child = fork();
            if (child == 0) {
			    sendFile(serverfd, clientAddr, clientAddrLength, req);
                exit(EXIT_SUCCESS);
            }
		}
	}

    // Free resources
    free(requestLine);
    free(timeBuffer);
    free(output);

	exit(EXIT_SUCCESS);
}

/*
 * This method the request made by the user for simple sanity. If any information
 * is missing then a "Bad Request" is returned. If the file is missing then a
 * "Not Found" is returned, otherwise a "OK" is returned.
 * 
 * The response messages are handled based on status code. The request is returned
 * with a status code always.
 */

http formRequest(char *buffer) {
	char *blocks[3];
	char tmp[1024];
    memset(tmp, 0, 1024);
	strncpy(tmp, buffer, strlen(buffer)); // copy the string because the tokenizer modifies it
	parse(tmp, blocks, " ");

    // setup the clean request
	http req;
	req.request = blocks[0];
	req.uri = blocks[1];
	req.version = blocks[2];


    // if anything is empty then fail
    if (blocks[0] == NULL || blocks[1] == NULL || blocks[2] == NULL) {
        req.uri = NULL;        
        req.statusCode = 400;
    } 

	for (int i = 0; i < strlen(req.request); i++)
		req.request[i] = toupper(req.request[i]);

	for (int i = 0; i < strlen(req.version); i++)
		req.version[i] = toupper(req.version[i]);
    
    if (strcmp(req.request, "GET") != 0) { // only handle get requests
        req.statusCode = 400;
    } else if (strcmp(req.version, "HTTP/1.0") != 0) { // only handle http/1.0
        req.statusCode = 400;
    } else { // if it is a get request and http/1.0, check if the file exists
        strcat(homedir, req.uri);
        if (checkDirExists(homedir)) { // special case if uri ends in /
            char *file = "index.html";
            strcat(req.uri, file);
            if (checkFileExists(strcat(homedir, file))) { // check for an index.html file only
                req.statusCode = 200;
            } else {
                req.statusCode = 404;
            }
        } else if (checkFileExists(homedir)) { // find the file
            req.statusCode = 200;
        } else {
            req.statusCode = 404;
        }
        homedir = getcwd(homedir, strlen(homedir));
    }
    
    req.version = "HTTP/1.0";   

    // messages set based on status code
    if (req.statusCode == 400) {
        req.response = "Bad Request";        
    } else if (req.statusCode == 404) {
        req.response = "Not Found";
    } else if (req.statusCode == 200) {
        req.response = "OK";
    }

	return req;
}

/*
 * This method sends the response header and the requested file to the client.
 * If the file doesn't exist then only the header is sent.
 */

void sendFile(int serverfd, struct sockaddr_in clientAddr, socklen_t clientAddrLength, http req) {
    char fin[100], header[50];
    memset(fin, 0, 100);

    // create the header and send it
    sprintf(header, "%s %d %s%s", req.version, req.statusCode, req.response, DELIMITER);
    sendto(serverfd, header, strlen(header), 0, (struct sockaddr *) &clientAddr, clientAddrLength);

    // if the request wasn't good then leave
    if (req.statusCode != 200) {
        return;
    }

    strcat(homedir, req.uri);

    FILE *fp = fopen(homedir, "rb");

    while(true) { // send the file in chunks
        if (fgets(fin, sizeof(fin), fp) != NULL) {
            sendto(serverfd, fin, strlen(fin), 0, (struct sockaddr *) &clientAddr, clientAddrLength);
        } else { // send an empty line once all the chunks are sent
            sendto(serverfd, DELIMITER, strlen(DELIMITER), 0, (struct sockaddr *) &clientAddr, clientAddrLength);
            break;
        }
    }
    
    fclose(fp);

    homedir = getcwd(homedir, strlen(homedir));
}
